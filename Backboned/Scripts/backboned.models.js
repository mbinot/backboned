var __extends = this.__extends || function (d, b) {
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
}
var backboned;
(function (backboned) {
    (function (models) {
        var Address = (function (_super) {
            __extends(Address, _super);
            function Address() {
                _super.apply(this, arguments);

            }
            Address.prototype.defaults = function () {
                return {
                    Street: 'new',
                    Number: 1
                };
            };
            Address.prototype.initialize = function () {
                console.log('initializing model Address');
            };
            return Address;
        })(Backbone.Model);
        models.Address = Address;        
        var AddressCollection = (function (_super) {
            __extends(AddressCollection, _super);
            function AddressCollection(models, opts) {
                        _super.call(this, models, opts);
                this.model = Address;
                this.initalize();
            }
            AddressCollection.prototype.initalize = function () {
                console.log('initializing model AddressCollection: ');
                this.on('add', this.promoteChange);
                this.on('remove', this.promoteChange);
                this.on('reset', this.promoteChange);
                this.on('change', this.promoteChange);
            };
            AddressCollection.prototype.promoteChange = function (a, b) {
                if(this._parent != undefined) {
                    this._parent.trigger('change', item);
                }
            };
            return AddressCollection;
        })(Backbone.Collection);
        models.AddressCollection = AddressCollection;        
        var Person = (function (_super) {
            __extends(Person, _super);
            function Person() {
                _super.apply(this, arguments);

                this.url = "/home/save";
            }
            Person.prototype.defaults = function () {
                return {
                    Name: '',
                    Age: 0,
                    Addresses: []
                };
            };
            Person.prototype.initialize = function () {
                console.log('initializing model Person');
                this.id = this.get("Name");
                this.set({
                    Addresses: new AddressCollection(this.get('Addresses'))
                });
                var addresses = this.get('Addresses');
                if(addresses != undefined) {
                    addresses._parent = this;
                }
            };
            Person.prototype.validate = function (attr) {
                if(attr.Name.length == 0) {
                    return "We DO need a name!";
                }
                if(attr.Age == undefined || attr.Age.length == 0) {
                    return "We DO need the age!";
                }
                return null;
            };
            return Person;
        })(Backbone.Model);
        models.Person = Person;        
    })(backboned.models || (backboned.models = {}));
    var models = backboned.models;

})(backboned || (backboned = {}));

