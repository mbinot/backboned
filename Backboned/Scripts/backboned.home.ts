/// <reference path="knockback.d.ts" />
/// <reference path="knockout.d.ts" />

module backboned.home {
    export class ViewModel extends kb.ViewModel {
        private _options: any;
        welcome;
        isDirty = ko.observable(false);
        isEditMode = ko.observable(false);
        Name: knockout.koObservableAny;

        //
        //
        //
        constructor (model, options) {
            super(model, options);
            this._options = options;

            this.welcome = ko.computed(() => { return 'Hello ' + this.Name() + "!"; });
            model.on("error", (item, message) => { this.showMessage(message, true); });
            model.on("change", (item, message) => {
              this.isDirty(true);
              this.showMessage("something changed");
            });
        }

        //
        // Shows a message using writer defined in options (if any).
        //
        showMessage (message: string, isError?: bool) {
          if (this._options.writer != undefined) {
            this._options.writer.showMessage(message, isError);
          }
        }

        //
        // Adds (creates) a new address.
        //
        addAddress() {
            var addresses = this.model().get('Addresses');
            var newItem = new addresses.model;
            addresses.push(newItem);
        }

        //
        // Removes a certain address
        //
        removeAddress(adr : any) {
            var addresses = this.model().get('Addresses');
            var item = addresses.find(function (i) { return i.get('Street') == adr.Street() && i.get('Number') == adr.Number(); });
            addresses.remove(item);
        }

        //
        // Saves the model (if we are in edit-mode)
        //
        save(e) {
            if (!this.isEditMode()) {
                return;
            }

            var myModel = e.model();
            myModel.save(null, this.getSaveOptions());
            e.isDirty(myModel.hasChanged());
        }

        //
        // Provides backbone-SaveOptions (Success-/Error-Handlers)
        //
        getSaveOptions() {
            return {
                success: () => { this.showMessage('* saved *'); },
                error: () => { this.showMessage('* ouuuh. failed! *', true); }
            }
        };

    }
}
