﻿/// <reference path="backbone.js" />
/// <reference path="knockback.js" />
/// <reference path="knockout-2.1.0.debug.js" />
var backboned = backboned || {};
backboned.models = backboned.models || { };
backboned.home = backboned.home || {};


/*
//---------------
// Models
//---------------

// Person (Model)
// A simple model that consists of two fields and a nested model.
backboned.models.Person = Backbone.Model.extend({
  // URL to persist the model
  url: "/Home/Save",
  defauls: { Name: '', Age: 0, Addresses:[] },
  
  initialize: function (model, options) {
    this.id = this.get("Name");
    this.set({
      Addresses: new backboned.models.AddressCollection(this.get('Addresses'))
    });
    var addresses = this.get('Addresses');
    if (addresses != undefined)
      addresses._parent = this;
  },
  
  validate: function (attr) {
    if (attr.Name.length == 0) {
      return "We DO need a name!";
    }
    
    if (attr.Age == undefined || attr.Age.length == 0) {
      return "We DO need the age!";
    }
    
    return null;
  }
});

// Address (model)
// A simple model that consists of two fields.
backboned.models.Address = Backbone.Model.extend({
  defaults: { Street: 'new', Number: 1},
});

// AddressCollection (model)
// A collection of 'Address'.
backboned.models.AddressCollection = Backbone.Collection.extend({
  model: backboned.models.Address,
  initialize: function () {
    console.log('initializing model AddressCollection: ');
    this.promoteChange = function (a, b) {
      if (this._parent != undefined) {
        this._parent.trigger('change', item);
      }
    };
    this.on('add', this.promoteChange);
    this.on('remove', this.promoteChange);
    this.on('reset', this.promoteChange);
    this.on('change', this.promoteChange);
  }
});

*/

//---------------
// ViewModels
//---------------

/*
backboned.common = backboned.common || {};
backboned.common.MessageViewModel = kb.ViewModel.extend({
  constructor: function (model, options) {
    console.log('initialize on MessageViewModel');
    kb.ViewModel.prototype.constructor.call(this, model, options);
    this.isError = ko.observable(false);
    this.message = ko.observable('');
    this.showMessage = function(message, isError) {
      this.message('');
      this.isError = (isError != undefined && isError);
      this.message(message);
    };
  }
});

backboned.home.ViewModel = kb.ViewModel.extend({
  // **
  // Constructor
  // **
  constructor: function (model, options) {
    var _this = this;

    this._options = options;
    
     //Call base constructor. This will define an observable for every property
     //of model.
    kb.ViewModel.prototype.constructor.call(this, model, options);

    // or set up manually for better control and model hierarchy
    //this.Name = kb.observable(model, 'Name');
    //this.Age = kb.observable(model, 'Age');
    
    // We might also want to add some more observables...
    this.welcome = ko.dependentObservable(function () { return 'Hello ' + this.Name() + "!"; }, this);
    this.isDirty = ko.observable(false);
    this.isEditMode = ko.observable(false);

    this.showMessage = function(message, isError) {
      if (_this._options.writer != undefined) {
        _this._options.writer.showMessage(message, isError);
      }
    };
    
    // Define success and error methods for model-operations
    this.saveOptions = {
      success: function() { _this.showMessage('* saved *'); },
      error: function() { _this.showMessage('* ouuuh. failed! *', true); }
    };

    this.addAddress = function (viewModel) {
      var addresses = _this.model().get('Addresses');
      var newItem = new addresses.model;
      addresses.push(newItem);
    };

    this.removeAddress = function(adr) {
      var addresses = _this.model().get('Addresses');
      var item = addresses.find(function (i) { return i.get('Street') == adr.Street() && i.get('Number') == adr.Number(); });
      addresses.remove(item);
    };

    model.on("error", function (item, message) { _this.showMessage(message, true); });
    model.on("change", function (item, message) {
      _this.isDirty(true);
      _this.showMessage("something changed");
    });
    return this;
  },
  
  // **
  // Persist the current model (i.e. post data to the server)
  // **
  save: function (e) {
    if (!this.isEditMode()) {
      return;
    }

    var myModel = e.model();
    myModel.save(null, this.saveOptions);
    e.isDirty(myModel.hasChanged());
  }
});
*/

/*
// **
// Knockout-Binding-Handler that fades elements in and out.
// Used to display the fading save-/error-message.
// **
ko.bindingHandlers.fadingMessage = {
  init: function (element, valueAccessor) {
    var value = valueAccessor();
    var unwrapped = ko.utils.unwrapObservable(value);
    $(element).toggle(unwrapped != '');
  },
  
  update: function (element, valueAccessor) {
    var value = valueAccessor();
    var unwrapped = ko.utils.unwrapObservable(value);
    
    // Do not fade if value is emtpy.
    if (unwrapped == undefined || unwrapped == null || unwrapped == '')
      return;
    
    $(element).fadeIn('slow', function () {
      $(this).delay(1000).fadeOut('slow');
    });
  }
};
*/