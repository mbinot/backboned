/// <reference path="knockout.d.ts" />
/// <reference path="knockback.d.ts" />
module backboned.common {
    export interface IMessageWriter {
        showMessage(message: string, isError: bool);
    }

    export class MessageViewModel extends kb.ViewModel implements IMessageWriter {
        private message = ko.observable('');
        private isError = ko.observable(false);

        constructor (model, options) {
            super(model, options);
            console.log('initialize on MessageViewModel');
        }

        showMessage(message: string, isError? : bool) {
              this.message('');
              this.isError(isError);
              this.message(message);
        }
    }
}

// **
// Knockout-Binding-Handler that fades elements in and out.
// Used to display the fading save-/error-message.
// **
ko.bindingHandlers.fadingMessage = {
  init: function (element, valueAccessor) {
    var value = valueAccessor();
    var unwrapped = ko.utils.unwrapObservable(value);
    $(element).toggle(unwrapped != '');
  },
  
  update: function (element, valueAccessor) {
    var value = valueAccessor();
    var unwrapped = ko.utils.unwrapObservable(value);
    
    // Do not fade if value is emtpy.
    if (unwrapped == undefined || unwrapped == null || unwrapped == '')
      return;
    
    $(element).fadeIn('slow', function () {
      $(this).delay(1000).fadeOut('slow');
    });
  }
};
