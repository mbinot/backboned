var __extends = this.__extends || function (d, b) {
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
}
var backboned;
(function (backboned) {
    (function (home) {
        var ViewModel = (function (_super) {
            __extends(ViewModel, _super);
            function ViewModel(model, options) {
                var _this = this;
                        _super.call(this, model, options);
                this.isDirty = ko.observable(false);
                this.isEditMode = ko.observable(false);
                this._options = options;
                this.welcome = ko.computed(function () {
                    return 'Hello ' + _this.Name() + "!";
                });
                model.on("error", function (item, message) {
                    _this.showMessage(message, true);
                });
                model.on("change", function (item, message) {
                    _this.isDirty(true);
                    _this.showMessage("something changed");
                });
            }
            ViewModel.prototype.showMessage = function (message, isError) {
                if(this._options.writer != undefined) {
                    this._options.writer.showMessage(message, isError);
                }
            };
            ViewModel.prototype.addAddress = function () {
                var addresses = this.model().get('Addresses');
                var newItem = new addresses.model();
                addresses.push(newItem);
            };
            ViewModel.prototype.removeAddress = function (adr) {
                var addresses = this.model().get('Addresses');
                var item = addresses.find(function (i) {
                    return i.get('Street') == adr.Street() && i.get('Number') == adr.Number();
                });
                addresses.remove(item);
            };
            ViewModel.prototype.save = function (e) {
                if(!this.isEditMode()) {
                    return;
                }
                var myModel = e.model();
                myModel.save(null, this.getSaveOptions());
                e.isDirty(myModel.hasChanged());
            };
            ViewModel.prototype.getSaveOptions = function () {
                var _this = this;
                return {
                    success: function () {
                        _this.showMessage('* saved *');
                    },
                    error: function () {
                        _this.showMessage('* ouuuh. failed! *', true);
                    }
                };
            };
            return ViewModel;
        })(kb.ViewModel);
        home.ViewModel = ViewModel;        
    })(backboned.home || (backboned.home = {}));
    var home = backboned.home;

})(backboned || (backboned = {}));

