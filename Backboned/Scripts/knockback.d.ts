﻿/// <reference path="backbone.d.ts" />
declare module kb {
    export class ViewModel {
        constructor (model? , options? );
        model(): Backbone.Model;
    }
}