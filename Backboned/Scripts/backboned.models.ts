/// <reference path="backbone.d.ts" />

//---------------
// Models
//---------------
module backboned.models {

    //---------------
    // Address
    //---------------
    export class Address extends Backbone.Model {
        defaults() {
            return { Street: 'new', Number: 1 };
        }

        initialize() {
            console.log('initializing model Address');
        }
    }

    //---------------
    // AddressCollection
    //---------------
    export class AddressCollection extends Backbone.Collection {
        model = Address;
        private _parent: Person;

        constructor (models?, opts?) {
            super(models, opts);
            this.initalize();
        }

        initalize() {
            console.log('initializing model AddressCollection: ');
            this.on('add', this.promoteChange);
            this.on('remove', this.promoteChange);
            this.on('reset', this.promoteChange);
            this.on('change', this.promoteChange);
        }

        promoteChange(a, b) {
            if (this._parent != undefined) {
                this._parent.trigger('change', item);
            }
        };
    }

    //---------------
    // Person
    //---------------
    export class Person extends Backbone.Model {
        url: string = "/home/save";
        defaults() {
            return { Name: '', Age: 0, Addresses: [] };
        }

        initialize() {
            console.log('initializing model Person');
            this.id = this.get("Name");
            this.set({
                Addresses: new AddressCollection(this.get('Addresses'))
            });
            var addresses = this.get('Addresses');
            if (addresses != undefined)
              addresses._parent = this;
        }

        validate(attr) {
            if (attr.Name.length == 0) {
              return "We DO need a name!";
            }
    
            if (attr.Age == undefined || attr.Age.length == 0) {
              return "We DO need the age!";
            }
    
            return null;
        }
    }
}
