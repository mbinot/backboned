var __extends = this.__extends || function (d, b) {
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
}
var backboned;
(function (backboned) {
    (function (common) {
        var MessageViewModel = (function (_super) {
            __extends(MessageViewModel, _super);
            function MessageViewModel(model, options) {
                        _super.call(this, model, options);
                this.message = ko.observable('');
                this.isError = ko.observable(false);
                console.log('initialize on MessageViewModel');
            }
            MessageViewModel.prototype.showMessage = function (message, isError) {
                this.message('');
                this.isError(isError);
                this.message(message);
            };
            return MessageViewModel;
        })(kb.ViewModel);
        common.MessageViewModel = MessageViewModel;        
    })(backboned.common || (backboned.common = {}));
    var common = backboned.common;

})(backboned || (backboned = {}));

ko.bindingHandlers.fadingMessage = {
    init: function (element, valueAccessor) {
        var value = valueAccessor();
        var unwrapped = ko.utils.unwrapObservable(value);
        $(element).toggle(unwrapped != '');
    },
    update: function (element, valueAccessor) {
        var value = valueAccessor();
        var unwrapped = ko.utils.unwrapObservable(value);
        if(unwrapped == undefined || unwrapped == null || unwrapped == '') {
            return;
        }
        $(element).fadeIn('slow', function () {
            $(this).delay(1000).fadeOut('slow');
        });
    }
};
