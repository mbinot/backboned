﻿using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using Backboned.Models;

namespace Backboned.Controllers
{
  public class HomeController : Controller
  {
    // Fakestore. This is just a demo...
    public static Person Store;

    //
    // GET: /Home/

    public ActionResult Index()
    {
      if (Store == null)
      {
        Store = new Person
                  {
                    Name = "Alfredo",
                    Age = 25,
                    Addresses = new List<Address> { new Address { Street = "Wallstreet", Number = "1a" } }
                  };
      }
      var viewModel = Store;
      return View(viewModel);
    }

    public ActionResult Save(Person person)
    {
      Store = person;
      return new HttpStatusCodeResult(HttpStatusCode.OK);
    }
  }
}