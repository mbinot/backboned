﻿using System.Collections.Generic;

namespace Backboned.Models
{
  public class Person
  {
    public string Name { get; set; }
    public int Age { get; set; }
    public IList<Address> Addresses { get; set; }
  }
}