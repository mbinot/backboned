﻿namespace Backboned.Models
{
  public class Address
  {
    public string Street { get; set; }
    public string Number { get; set; }
  }
}