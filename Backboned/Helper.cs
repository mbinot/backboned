﻿
using System.Web.Script.Serialization;

namespace Backboned
{
  public static class Helper
  {
     public static string JsonSerialize(this object model)
     {
       var serializier = new JavaScriptSerializer();
       return serializier.Serialize(model);
     }
  }
}